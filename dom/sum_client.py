# -*- encoding: utf-8 -*-

import socket

print('This program takes 2 numbers and send it to server to get their sum')

# Tworzenie gniazda TCP/IP
client_socket = socket.socket()

# Połączenie z gniazdem nasłuchującego serwera
server_address = ('194.29.175.240', 31006)
client_socket.connect(server_address)
print('Connected to server')

try:
    # Wysłanie danych
    number1 = raw_input("Enter first number value: ")
    client_socket.send(str(number1))
    print('Sent first number to server')

    number2 = raw_input("Enter second number value: ")
    client_socket.send(str(number2))
    print('Sent second number to server')


    # Wypisanie odpowiedzi
    response = client_socket.recv(1024)
    print('Receiving response from server:')
    print(str(number1) + ' + ' + str(number2) + ' = ' + response)

finally:
    # Zamknięcie połączenia
    client_socket.close()
    pass
# -*- encoding: utf-8 -*-

import socket

# Ustawienie licznika na zero
a = 0

# Tworzenie gniazda TCP/IP
gniazdo = socket.socket()

# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31006)
gniazdo.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
gniazdo.listen(1)

while True:
    # Czekanie na połączenie
    connection, client_address = gniazdo.accept()

    # Podbicie licznika
    a += 1

    try:
        # Wysłanie wartości licznika do klienta
        connection.sendall(str(a))
        pass

    finally:
        # Zamknięcie połączenia
        connection.close()
        pass
print("Done")
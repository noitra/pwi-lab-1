# -*- encoding: utf-8 -*-

import socket

# Tworzenie gniazda TCP/IP
gniazdo = socket.socket()
# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31006)
gniazdo.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
gniazdo.listen(1)

while True:
    # Czekanie na połączenie
    connection, client_address = gniazdo.accept()
    try:
        # Odebranie danych i odesłanie ich spowrotem
        dane = connection.recv(1024)
        connection.send(str(dane))
        pass

    finally:
        # Zamknięcie połączenia
        connection.close()
        pass
print("Done")